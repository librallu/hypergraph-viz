export interface Instance {
  y: string[],
  x: string[],
  r: {
    [propname: string]: any
  }
}

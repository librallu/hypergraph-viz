/*
  Used to solve compatibility issues with Typescript and Vue.
*/
declare module "*.vue" {
  import Vue from 'vue'
  export default Vue
}
